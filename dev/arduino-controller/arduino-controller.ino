#include <JC_Button.h>

/*MIDI messages
midi_message = {channel, sine, value, square, value, sawtooth, value, triangle, value} */
byte midi_controller[] = {0xB0, 0x00, 0x00, 0x01, 0x00, 0x02, 0x00, 0x03, 0x00};

/*controllers and indicators */
int pot[4];
byte pot_pin[4] = {A0, A1, A2, A3};
int button[5] = {0, 0, 0, 0, 0};
int button_pin[5] = {13, 12, 11, 10, 9};
int led_pin[5] = {8, 7, 6, 5, 4};

ToggleButton b1(button_pin[0]), b2(button_pin[1]), b3(button_pin[2]), b4(button_pin[3]), b5(button_pin[4]);
ToggleButton buttons[] = {b1,b2,b3,b4,b5};

void setup()
{
    for (int i = 0; i < 5; i++)
    {
        buttons[i].begin();
        pinMode(led_pin[i], OUTPUT);
    }
    Serial.begin(9600);
}

void loop()
{
    readPots();
    normalizePots();
    readButtons();
    buttonStateController();
    sendMidiMessage();
    //sendAsciiMessage();
    delay(5);
    //printButtonState();
    //printPotState();
}

void sendMidiMessage()
{
    for (int i = 0; i < 4; i++)
    {
        midi_controller[2 + 2 * i] = button[i] * pot[i];
    }
    Serial.write(midi_controller, sizeof(midi_controller));
}

void sendAsciiMessage()
{
    for (int i = 0; i < 4; i++)
    {
        midi_controller[2 + 2 * i] = button[i] * pot[i];
    }
    for (int i = 0; i < 9; i++)
    {
        Serial.print(midi_controller[i]);
        Serial.print(" \t");
    }
    Serial.println(""); 
}

void buttonStateController()
{
    for (int i = 0; i < 5; i++)
    {
        if (buttons[i].toggleState())
        {
            button[i] = 1;
            digitalWrite(led_pin[i], HIGH);
        }
        else
        {
            button[i] = 0;
            digitalWrite(led_pin[i], LOW);
        }
    }
}

void printPotState()
{
    Serial.print("Pot state: ");
    for (size_t i = 0; i < 4; i++)
    {
        Serial.print(pot[i]);
        Serial.print(" \r");
    }
    Serial.println("");
}

void printButtonState()
{
    Serial.print("Buttons state: ");
    for (int i = 0; i < 5; i++)
    {
        Serial.print(button[i]);
        Serial.print(" \r");
    }
    Serial.println("");
}

void readPots()
{ //read the value of the pots
    for (int i = 0; i < 4; i++)
    {
        pot[i] = analogRead(pot_pin[i]);
    }
}

void normalizePots()
{
    for (int i = 0; i < 4; i++)
    {
        pot[i] = normalizePot2Hex(pot[i]);
    }
}

void readButtons()
{ //read and change the state of the buttons
    for (int i = 0; i < 5; i++)
    {
        buttons[i].read();
        buttons[i].toggleState();
    }
}

int normalizePot2Hex(int pot_dec)
{ //Normalize dec input to hex output
    int pot_hex;
    pot_hex = (pot_dec / 1023.0) * 127.0; //[0-1023] --> [0-127]
    return pot_hex;
}
