#include <JC_Button.h>
#include <Filters.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

void setup()
{
  initVariables();
}

void loop()
{
  readInputs();
  filterAnalogInputs();
  getSectionState();
  writeOutputs();
  ParametersValues();
  sendParameters();
}
