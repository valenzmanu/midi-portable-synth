/*----------------------------------------------------
  OLED screen variables
-----------------------------------------------------*/
#define HEIGHT 64
#define WIDTH 128
#define OLED_RESET 4
Adafruit_SSD1306 oled(WIDTH, HEIGHT, &Wire, OLED_RESET);

/*----------------------------------------------------
  Input Pins
-----------------------------------------------------*/
//Potenciometers
#define volumePotPin A8
#define gainPotPin A9
#define adsrPotPin A10
#define filtersPotPin A11
#define oscSignalPotPin A12
#define oscDetunePotPin A13
#define delayPotPin A14
#define reverbPotPin A15
//Buttons
#define adsrSelectorButtonPin 48
#define filtersTypeButtonPin 50
#define filtersSelectorButtonPin 52
#define oscSignalSelectorButtonPin 46
#define oscDetuneSelectorButtonPin 44
#define delayBypassButtonPin 53
#define delaySelectorButtonPin 51
#define reverbBypassButtonPin 49
#define reverbFreezeButtonPin 47
#define reverbSelectorButtonPin 45
/*----------------------------------------------------
  Output Pins
-----------------------------------------------------*/
//Indicator LEDs
#define adsrAttackLEDpin 9
#define adsrDecayLEDpin 11
#define adsrSustainLEDpin 10
#define adsrReleaseLEDpin 12
#define filtersCutoffLEDpin 2
#define filtersLfoFreqLEDpin 3
#define filtersCenterFreqLEDpin 4
#define filtersResonanceLEDpin 5
#define filtersModulationLEDpin 6
#define filtersSelector1LEDpin 8
#define filtersSelector2LEDpin 7
#define oscSinSignalLEDpin 42
#define oscSawSignalLEDpin 40
#define oscSqrSignalLEDpin 38
#define oscSinDetuneLEDpin 36
#define oscSawDetuneLEDpin 34
#define oscSqrDetuneLEDpin 32
#define delayMixLEDpin 28
#define delayFeedbackLEDpin 31
#define delayTempoLEDpin 30
#define delayBypassLEDpin 33
#define reverbRoomsizeLEDpin 35
#define reverbDampingLEDpin 37
#define reverbWidthLEDpin 39
#define reverbWetLEDpin 41
#define reverbDryLEDpin 43
#define reverbBypassLEDpin 29
#define reverbFreezeLEDpin 27

int ledpins[] = {adsrAttackLEDpin,
                 adsrDecayLEDpin,
                 adsrSustainLEDpin,
                 adsrReleaseLEDpin,
                 filtersCutoffLEDpin,
                 filtersLfoFreqLEDpin,
                 filtersCenterFreqLEDpin,
                 filtersResonanceLEDpin,
                 filtersModulationLEDpin,
                 filtersSelector1LEDpin,
                 filtersSelector2LEDpin,
                 oscSinSignalLEDpin,
                 oscSawSignalLEDpin,
                 oscSqrSignalLEDpin,
                 oscSinDetuneLEDpin,
                 oscSawDetuneLEDpin,
                 oscSqrDetuneLEDpin,
                 delayMixLEDpin,
                 delayFeedbackLEDpin,
                 delayTempoLEDpin,
                 delayBypassLEDpin,
                 reverbRoomsizeLEDpin,
                 reverbDampingLEDpin,
                 reverbWidthLEDpin,
                 reverbWetLEDpin,
                 reverbDryLEDpin,
                 reverbBypassLEDpin,
                 reverbFreezeLEDpin};

/*----------------------------------------------------
  Input Variables
-----------------------------------------------------*/
//Potenciometers
#define volumePotIndex 0
#define gainPotIndex 1
#define adsrPotIndex 2
#define filtersPotIndex 3
#define oscSignalPotIndex 4
#define oscDetunePotIndex 5
#define delayPotIndex 6
#define reverbPotIndex 7
int potenciometers[8];
int potenciometersPins[] = {volumePotPin,
                            gainPotPin,
                            adsrPotPin,
                            filtersPotPin,
                            oscSignalPotPin,
                            oscDetunePotPin,
                            delayPotPin,
                            reverbPotPin};
//Buttons
#define adsrSelectorButtonIndex 0
#define filtersTypeButtonIndex 1
#define filtersSelectorButtonIndex 2
#define oscSignalSelectorButtonIndex 3
#define oscDetuneSelectorButtonIndex 4
#define delayBypassButtonIndex 5
#define delaySelectorButtonIndex 6
#define reverbBypassButtonIndex 7
#define reverbFreezeButtonIndex 8
#define reverbSelectorButtonIndex 9
#define debounceTime 50
Button adsrSelectorButton(adsrSelectorButtonPin, debounceTime),
    filtersTypeButton(filtersTypeButtonPin, debounceTime),
    filtersSelectorButton(filtersSelectorButtonPin, debounceTime),
    oscSignalSelectorButton(oscSignalSelectorButtonPin, debounceTime),
    oscDetuneSelectorButton(oscDetuneSelectorButtonPin, debounceTime),
    delayBypassButton(delayBypassButtonPin, debounceTime),
    delaySelectorButton(delaySelectorButtonPin, debounceTime),
    reverbBypassButton(reverbBypassButtonPin, debounceTime),
    reverbFreezeButton(reverbFreezeButtonPin, debounceTime),
    reverbSelectorButton(reverbSelectorButtonPin, debounceTime);
Button buttons[] = {adsrSelectorButton,
                    filtersTypeButton,
                    filtersSelectorButton,
                    oscSignalSelectorButton,
                    oscDetuneSelectorButton,
                    delayBypassButton,
                    delaySelectorButton,
                    reverbBypassButton,
                    reverbFreezeButton,
                    reverbSelectorButton};

/*----------------------------------------------------
  Analog Inputs Filters
-----------------------------------------------------*/
#define changeThreshold 5

//Define cutoff frequencies
float lowpassCutoffFreq = 0.5;
float highpassCutoffFreq = 100;

//Define Filters for Potenciometers
FilterOnePole volLPF(LOWPASS, lowpassCutoffFreq);
FilterOnePole volHPF(HIGHPASS, lowpassCutoffFreq);
FilterOnePole gainLPF(LOWPASS, lowpassCutoffFreq);
FilterOnePole gainHPF(HIGHPASS, lowpassCutoffFreq);
FilterOnePole adsrLPF(LOWPASS, lowpassCutoffFreq);
FilterOnePole adsrHPF(HIGHPASS, lowpassCutoffFreq);
FilterOnePole filtersLPF(LOWPASS, lowpassCutoffFreq);
FilterOnePole filtersHPF(HIGHPASS, lowpassCutoffFreq);
FilterOnePole oscLPF(LOWPASS, lowpassCutoffFreq);
FilterOnePole oscHPF(HIGHPASS, lowpassCutoffFreq);
FilterOnePole detuneLPF(LOWPASS, lowpassCutoffFreq);
FilterOnePole detuneHPF(HIGHPASS, lowpassCutoffFreq);
FilterOnePole delayLPF(LOWPASS, lowpassCutoffFreq);
FilterOnePole delayHPF(HIGHPASS, lowpassCutoffFreq);
FilterOnePole reverbLPF(LOWPASS, lowpassCutoffFreq);
FilterOnePole reverbHPF(HIGHPASS, lowpassCutoffFreq);

//Put filters in arrays
FilterOnePole potsFilterContainerLPF[] = {volLPF,
                                          gainLPF,
                                          adsrLPF,
                                          filtersLPF,
                                          oscLPF,
                                          detuneLPF,
                                          delayLPF,
                                          reverbLPF};

FilterOnePole potsFilterContainerHPF[] = {volHPF,
                                          gainHPF,
                                          adsrHPF,
                                          filtersHPF,
                                          oscHPF,
                                          detuneHPF,
                                          delayHPF,
                                          reverbHPF};

int filteredPotenciometersLPF[8];
int filteredPotenciometersHPF[8];

/*----------------------------------------------------
  Section Counters
-----------------------------------------------------*/
#define adsrParameters 5
#define filterParameters 6
#define filterTypes 3
#define oscSignals 4
#define oscDetunes 4
#define delayParameters 4
#define delayBypass 2
#define reverbParameters 6
#define reverbBypass 2
#define reverbFreeze 2
int adsrCounter = 0;
int filtersParameterCounter = 0;
int filtersTypeCounter = 0;
int oscSignalCounter = 0;
int oscDetuneCounter = 0;
int delayParameterCounter = 0;
int delayBypassCounter = 0;
int reverbParameterCounter = 0;
int reverbBypassCounter = 0;
int reverbFreezeCounter = 0;

/*----------------------------------------------------
  Parameters Last value
-----------------------------------------------------*/
#define lastValueShowTime 400

//ADSR
int adsrCounterLastValue = 0;
String attackLastValue = "0";
String decayLastValue = "0";
String sustainLastValue = "0";
String releaseLastValue = "0";

//Filters
int filtersParameterCounterLastValue = 0;
String cutoffLastValue = "0";
String lfoFreqLastValue = "0";
String centerFreqLastValue = "0";
String resonanceLastValue = "0";
String modulationLastValue = "0";

/*----------------------------------------------------
  Parameters to send Variables
-----------------------------------------------------*/
String param2Send = "";