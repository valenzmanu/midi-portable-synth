#include "variables.h"

void initVariables()
{
  //Serial Ports
  Serial.begin(115200);

  //Output Pins
  for (int i = 0; i < 28; i++)
  {
    pinMode(ledpins[i], OUTPUT);
    digitalWrite(ledpins[i], HIGH);
  }

  //OLED screen objects
  oled.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  oled.clearDisplay();
  sendScreenText("**********", "Welcome");
  delay(2000);
  oled.clearDisplay();
  sendScreenText("**********", "Loading");
  delay(2000);

  //Initialize buttons objects
  for (int i = 0; i < 10; i++)
  {
    buttons[i].begin();
  }

  //Turn Off LEDs
  for (int i = 0; i < 28; i++)
  {
    digitalWrite(ledpins[i], LOW);
  }
}

void ParametersValues()
{
  //Reset parameters
  param2Send = "";

  /*******************************************************
    Parameters that are always sent
   ******************************************************/
  //Filters Type Selector
  switch (filtersTypeCounter)
  {
  case 0:
    param2Send = param2Send + "filterTypePy 0, ";
    break;
  case 1:
    param2Send = param2Send + "filterTypePy 1, ";
    break;
  case 2:
    param2Send = param2Send + "filterTypePy 2, ";
    break;

  default:
    break;
  }

  //Delay Bypass
  switch (delayBypassCounter)
  {
  case 0:
    param2Send = param2Send + "bypassDelayPy 0, ";
    break;
  case 1:
    param2Send = param2Send + "bypassDelayPy 1, ";
    break;

  default:
    break;
  }

  //Reverb Bypass
  switch (reverbBypassCounter)
  {
  case 0:
    param2Send = param2Send + "bypassReverbPy 0, ";
    break;
  case 1:
    param2Send = param2Send + "bypassReverbPy 1, ";
    break;

  default:
    break;
  }

  //Reverb Freeze
  switch (reverbFreezeCounter)
  {
  case 0:
    param2Send = param2Send + "freezePy 0, ";
    break;
  case 1:
    param2Send = param2Send + "freezePy 1, ";
    break;

  default:
    break;
  }

  /*******************************************************
    Parameters sent in changes
   ******************************************************/

  //Volume
  if (abs(filteredPotenciometersHPF[volumePotIndex]) > changeThreshold)
  {
    param2Send = param2Send + "mVolumePy " + String(filteredPotenciometersLPF[volumePotIndex]) + ", ";
    sendScreenText("Volume:", String(int(filteredPotenciometersLPF[volumePotIndex] / 10.21)) + " %");
    return;
  }
  //Gain
  else if (abs(filteredPotenciometersHPF[gainPotIndex]) > changeThreshold)
  {
    param2Send = param2Send + "mainGainPy " + String(filteredPotenciometersLPF[gainPotIndex]) + ", ";
    sendScreenText("Gain:", String(int(filteredPotenciometersLPF[gainPotIndex] / 10.21)) + " %");
    return;
  }
  else
  {
    //ADSR
    switch (adsrCounter)
    {
    case 1:
      //ADSR
      if (adsrCounterLastValue != 1)
      {
        adsrCounterLastValue = 1;
        sendScreenText("Release:", releaseLastValue + "%");
        delay(lastValueShowTime);
      }
      else if (abs(filteredPotenciometersHPF[adsrPotIndex]) > changeThreshold)
      {
        param2Send = param2Send + "releasePy " + String(filteredPotenciometersLPF[adsrPotIndex]) + ", ";
        releaseLastValue = String(int(filteredPotenciometersLPF[adsrPotIndex] / 10.21));
        sendScreenText("Release:", releaseLastValue + " %");
        return;
      }
      break;

    case 2:
      if (adsrCounterLastValue != 2)
      {
        adsrCounterLastValue = 2;
        sendScreenText("Sustain:", sustainLastValue + " %");
        delay(lastValueShowTime);
      }
      else if (abs(filteredPotenciometersHPF[adsrPotIndex]) > changeThreshold)
      {
        param2Send = param2Send + "sustainPy " + String(filteredPotenciometersLPF[adsrPotIndex]) + ", ";
        sustainLastValue = String(int(filteredPotenciometersLPF[adsrPotIndex] / 10.21));
        sendScreenText("Sustain:", sustainLastValue + " %");
        return;
      }
      break;

    case 3:
      if (adsrCounterLastValue != 3)
      {
        adsrCounterLastValue = 3;
        sendScreenText("Decay:", decayLastValue + " %");
        delay(lastValueShowTime);
      }
      else if (abs(filteredPotenciometersHPF[adsrPotIndex]) > changeThreshold)
      {
        param2Send = param2Send + "decayPy " + String(filteredPotenciometersLPF[adsrPotIndex]) + ", ";
        decayLastValue = String(int(filteredPotenciometersLPF[adsrPotIndex] / 10.21));
        sendScreenText("Decay:", decayLastValue + " %");
        return;
      }
      break;

    case 4:
      if (adsrCounterLastValue != 4)
      {
        adsrCounterLastValue = 4;
        sendScreenText("Attack:", attackLastValue + " %");
        delay(lastValueShowTime);
      }
      else if (abs(filteredPotenciometersHPF[adsrPotIndex]) > changeThreshold)
      {
        param2Send = param2Send + "attackPy " + String(filteredPotenciometersLPF[adsrPotIndex]) + ", ";
        attackLastValue = String(int(filteredPotenciometersLPF[adsrPotIndex] / 10.21));
        sendScreenText("Attack:", attackLastValue + " %");
        return;
      }
      break;

    default:
      break;
    }

    //Filters parameters
    switch (filtersParameterCounter)
    {
    case 1:
      if (filtersCounterLastValue != 1)
      {
        filtersCounterLastValue = 4;
        sendScreenText("Attack:", attackLastValue + " %");
        delay(lastValueShowTime);
      }
      if (abs(filteredPotenciometersHPF[filtersPotIndex]) > changeThreshold)
      {
        param2Send = param2Send + "cutoffPy " + String(filteredPotenciometersLPF[filtersPotIndex]) + ", ";
        sendScreenText("Cutoff:", String(int(filteredPotenciometersLPF[filtersPotIndex])) + " Hz");
        return;
      }
      break;
    case 2:
      if (abs(filteredPotenciometersHPF[filtersPotIndex]) > changeThreshold)
      {
        param2Send = param2Send + "lfoFreqPy " + String(filteredPotenciometersLPF[filtersPotIndex]) + ", ";
        sendScreenText("LFO Freq:", String(int(filteredPotenciometersLPF[filtersPotIndex])) + " Hz");
        return;
      }
      break;
    case 3:
      if (abs(filteredPotenciometersHPF[filtersPotIndex]) > changeThreshold)
      {
        param2Send = param2Send + "centerFreqPy " + String(filteredPotenciometersLPF[filtersPotIndex]) + ", ";
        sendScreenText("Center Freq:", String(int(filteredPotenciometersLPF[filtersPotIndex])) + " Hz");
        return;
      }
      break;
    case 4:
      if (abs(filteredPotenciometersHPF[filtersPotIndex]) > changeThreshold)
      {
        param2Send = param2Send + "resonancePy " + String(filteredPotenciometersLPF[filtersPotIndex]) + ", ";
        sendScreenText("Resonance:", String(int(filteredPotenciometersLPF[filtersPotIndex] / 10.21)) + " %");
        return;
      }
      break;
    case 5:
      if (abs(filteredPotenciometersHPF[filtersPotIndex]) > changeThreshold)
      {
        param2Send = param2Send + "modAmountPy " + String(filteredPotenciometersLPF[filtersPotIndex]) + ", ";
        sendScreenText("Modulation", String(int(filteredPotenciometersLPF[filtersPotIndex] / 10.21)) + " %");
        return;
      }
      break;

    default:
      break;
    }

    //OSC
    switch (oscSignalCounter)
    {
    case 1:
      if (abs(filteredPotenciometersHPF[oscSignalPotIndex]) > changeThreshold)
      {
        param2Send = param2Send + "sqrGainPy " + String(filteredPotenciometersLPF[oscSignalPotIndex]) + ", ";
        sendScreenText("SQR:", String(int(filteredPotenciometersLPF[oscSignalPotIndex] / 10.21)) + " %");
        return;
      }
      break;
    case 2:
      if (abs(filteredPotenciometersHPF[oscSignalPotIndex]) > changeThreshold)
      {
        param2Send = param2Send + "sawGainPy " + String(filteredPotenciometersLPF[oscSignalPotIndex]) + ", ";
        sendScreenText("SAW:", String(int(filteredPotenciometersLPF[oscSignalPotIndex] / 10.21)) + " %");
        return;
      }
      break;
    case 3:
      if (abs(filteredPotenciometersHPF[oscSignalPotIndex]) > changeThreshold)
      {
        param2Send = param2Send + "sinGainPy " + String(filteredPotenciometersLPF[oscSignalPotIndex]) + ", ";
        sendScreenText("SIN:", String(int(filteredPotenciometersLPF[oscSignalPotIndex] / 10.21)) + " %");
        return;
      }
      break;

    default:
      break;
    }

    //Detune
    switch (oscDetuneCounter)
    {
    case 1:
      if (abs(filteredPotenciometersHPF[oscDetunePotIndex]) > changeThreshold)
      {
        param2Send = param2Send + "sqrDetPy " + String(filteredPotenciometersLPF[oscDetunePotIndex]) + ", ";
        sendScreenText("SQR DET:", String(int(filteredPotenciometersLPF[oscDetunePotIndex] / 10.21)) + " %");
        return;
      }
      break;
    case 2:
      if (abs(filteredPotenciometersHPF[oscDetunePotIndex]) > changeThreshold)
      {
        param2Send = param2Send + "sawDetPy " + String(filteredPotenciometersLPF[oscDetunePotIndex]) + ", ";
        sendScreenText("SAW DET:", String(int(filteredPotenciometersLPF[oscDetunePotIndex] / 10.21)) + " %");
        return;
      }
      break;
    case 3:
      if (abs(filteredPotenciometersHPF[oscDetunePotIndex]) > changeThreshold)
      {
        param2Send = param2Send + "sinDetPy " + String(filteredPotenciometersLPF[oscDetunePotIndex]) + ", ";
        sendScreenText("SIN DET:", String(int(filteredPotenciometersLPF[oscDetunePotIndex] / 10.21)) + " %");
        return;
      }
      break;

    default:
      break;
    }

    //Delay Parameters
    switch (delayParameterCounter)
    {
    case 1:
      if (abs(filteredPotenciometersHPF[delayPotIndex]) > changeThreshold)
      {
        param2Send = param2Send + "tempoDelayPy " + String(filteredPotenciometersLPF[delayPotIndex]) + ", ";
        sendScreenText("Tempo:", String(int(filteredPotenciometersLPF[delayPotIndex] / 10.21)) + " %");
        return;
      }
      break;
    case 2:
      if (abs(filteredPotenciometersHPF[delayPotIndex]) > changeThreshold)
      {
        param2Send = param2Send + "feedbackDelayPy " + String(filteredPotenciometersLPF[delayPotIndex]) + ", ";
        sendScreenText("Feedback:", String(int(filteredPotenciometersLPF[delayPotIndex] / 10.21)) + " %");
        return;
      }
      break;
    case 3:
      if (abs(filteredPotenciometersHPF[delayPotIndex]) > changeThreshold)
      {
        param2Send = param2Send + "mixDelayPy " + String(filteredPotenciometersLPF[delayPotIndex]) + ", ";
        sendScreenText("Mix:", String(int(filteredPotenciometersLPF[delayPotIndex] / 10.21)) + " %");
        return;
      }
      break;

    default:
      break;
    }

    //Reverb Parameters
    switch (reverbParameterCounter)
    {
    case 1:
      if (abs(filteredPotenciometersHPF[reverbPotIndex]) > changeThreshold)
      {
        param2Send = param2Send + "dryPy " + String(filteredPotenciometersLPF[reverbPotIndex]) + ", ";
        sendScreenText("Dry:", String(int(filteredPotenciometersLPF[reverbPotIndex] / 10.21)) + " %");
        return;
      }
      break;
    case 2:
      if (abs(filteredPotenciometersHPF[reverbPotIndex]) > changeThreshold)
      {
        param2Send = param2Send + "wetPy " + String(filteredPotenciometersLPF[reverbPotIndex]) + ", ";
        sendScreenText("Wet:", String(int(filteredPotenciometersLPF[reverbPotIndex] / 10.21)) + " %");
        return;
      }
      break;
    case 3:
      if (abs(filteredPotenciometersHPF[reverbPotIndex]) > changeThreshold)
      {
        param2Send = param2Send + "widthPy " + String(filteredPotenciometersLPF[reverbPotIndex]) + ", ";
        sendScreenText("Width:", String(int(filteredPotenciometersLPF[reverbPotIndex] / 10.21)) + " %");
        return;
      }
      break;
    case 4:
      if (abs(filteredPotenciometersHPF[reverbPotIndex]) > changeThreshold)
      {
        param2Send = param2Send + "dampingPy " + String(filteredPotenciometersLPF[reverbPotIndex]) + ", ";
        sendScreenText("Damping:", String(int(filteredPotenciometersLPF[reverbPotIndex] / 10.21)) + " %");
        return;
      }
      break;
    case 5:
      if (abs(filteredPotenciometersHPF[reverbPotIndex]) > changeThreshold)
      {
        param2Send = param2Send + "roomsizePy " + String(filteredPotenciometersLPF[reverbPotIndex]) + ", ";
        sendScreenText("Roomsize:", String(int(filteredPotenciometersLPF[reverbPotIndex] / 10.21)) + " %");
        return;
      }
      break;

    default:
      break;
    }
    sendScreenText("MEGA", "SYNTH");
  }
}

void getSectionState()
{
  if (buttons[adsrSelectorButtonIndex].wasPressed())
  {
    adsrCounter = (adsrCounter + 1) % adsrParameters;
  }
  if (buttons[filtersSelectorButtonIndex].wasPressed())
  {
    filtersParameterCounter = (filtersParameterCounter + 1) % filterParameters;
  }
  if (buttons[filtersTypeButtonIndex].wasPressed())
  {
    filtersTypeCounter = (filtersTypeCounter + 1) % filterTypes;
  }
  if (buttons[oscSignalSelectorButtonIndex].wasPressed())
  {
    oscSignalCounter = (oscSignalCounter + 1) % oscSignals;
  }
  if (buttons[oscDetuneSelectorButtonIndex].wasPressed())
  {
    oscDetuneCounter = (oscDetuneCounter + 1) % oscDetunes;
  }
  if (buttons[delaySelectorButtonIndex].wasPressed())
  {
    delayParameterCounter = (delayParameterCounter + 1) % delayParameters;
  }
  if (buttons[delayBypassButtonIndex].wasPressed())
  {
    delayBypassCounter = (delayBypassCounter + 1) % delayBypass;
  }
  if (buttons[reverbSelectorButtonIndex].wasPressed())
  {
    reverbParameterCounter = (reverbParameterCounter + 1) % reverbParameters;
  }
  if (buttons[reverbBypassButtonIndex].wasPressed())
  {
    reverbBypassCounter = (reverbBypassCounter + 1) % reverbBypass;
  }
  if (buttons[reverbFreezeButtonIndex].wasPressed())
  {
    reverbFreezeCounter = (reverbFreezeCounter + 1) % reverbFreeze;
  }
}

void readInputs()
{
  //Read potenciometers
  for (int i = 0; i < 8; i++)
  {
    potenciometers[i] = analogRead(potenciometersPins[i]);
  }

  //Read Buttons
  for (int i = 0; i < 10; i++)
  {
    buttons[i].read();
  }
}

void writeOutputs()
{
  //ADSR
  switch (adsrCounter)
  {
  case 1:
    digitalWrite(adsrReleaseLEDpin, HIGH);
    digitalWrite(adsrSustainLEDpin, LOW);
    digitalWrite(adsrDecayLEDpin, LOW);
    digitalWrite(adsrAttackLEDpin, LOW);
    break;
  case 2:
    digitalWrite(adsrReleaseLEDpin, LOW);
    digitalWrite(adsrSustainLEDpin, HIGH);
    digitalWrite(adsrDecayLEDpin, LOW);
    digitalWrite(adsrAttackLEDpin, LOW);
    break;
  case 3:
    digitalWrite(adsrReleaseLEDpin, LOW);
    digitalWrite(adsrSustainLEDpin, LOW);
    digitalWrite(adsrDecayLEDpin, HIGH);
    digitalWrite(adsrAttackLEDpin, LOW);
    break;
  case 4:
    digitalWrite(adsrReleaseLEDpin, LOW);
    digitalWrite(adsrSustainLEDpin, LOW);
    digitalWrite(adsrDecayLEDpin, LOW);
    digitalWrite(adsrAttackLEDpin, HIGH);
    break;

  default:
    digitalWrite(adsrReleaseLEDpin, LOW);
    digitalWrite(adsrSustainLEDpin, LOW);
    digitalWrite(adsrDecayLEDpin, LOW);
    digitalWrite(adsrAttackLEDpin, LOW);
    break;
  }

  //Filters
  switch (filtersParameterCounter)
  {
  case 1:
    digitalWrite(filtersModulationLEDpin, HIGH);
    digitalWrite(filtersResonanceLEDpin, LOW);
    digitalWrite(filtersCenterFreqLEDpin, LOW);
    digitalWrite(filtersLfoFreqLEDpin, LOW);
    digitalWrite(filtersCutoffLEDpin, LOW);
    break;
  case 2:
    digitalWrite(filtersModulationLEDpin, LOW);
    digitalWrite(filtersResonanceLEDpin, HIGH);
    digitalWrite(filtersCenterFreqLEDpin, LOW);
    digitalWrite(filtersLfoFreqLEDpin, LOW);
    digitalWrite(filtersCutoffLEDpin, LOW);
    break;
  case 3:
    digitalWrite(filtersModulationLEDpin, LOW);
    digitalWrite(filtersResonanceLEDpin, LOW);
    digitalWrite(filtersCenterFreqLEDpin, HIGH);
    digitalWrite(filtersLfoFreqLEDpin, LOW);
    digitalWrite(filtersCutoffLEDpin, LOW);
    break;
  case 4:
    digitalWrite(filtersModulationLEDpin, LOW);
    digitalWrite(filtersResonanceLEDpin, LOW);
    digitalWrite(filtersCenterFreqLEDpin, LOW);
    digitalWrite(filtersLfoFreqLEDpin, HIGH);
    digitalWrite(filtersCutoffLEDpin, LOW);
    break;
  case 5:
    digitalWrite(filtersModulationLEDpin, LOW);
    digitalWrite(filtersResonanceLEDpin, LOW);
    digitalWrite(filtersCenterFreqLEDpin, LOW);
    digitalWrite(filtersLfoFreqLEDpin, LOW);
    digitalWrite(filtersCutoffLEDpin, HIGH);
    break;

  default:
    digitalWrite(filtersModulationLEDpin, LOW);
    digitalWrite(filtersResonanceLEDpin, LOW);
    digitalWrite(filtersCenterFreqLEDpin, LOW);
    digitalWrite(filtersLfoFreqLEDpin, LOW);
    digitalWrite(filtersCutoffLEDpin, LOW);
    break;
    break;
  }

  //OSC
  switch (oscSignalCounter)
  {
  case 1:
    digitalWrite(oscSqrSignalLEDpin, HIGH);
    digitalWrite(oscSawSignalLEDpin, LOW);
    digitalWrite(oscSinSignalLEDpin, LOW);
    break;
  case 2:
    digitalWrite(oscSqrSignalLEDpin, LOW);
    digitalWrite(oscSawSignalLEDpin, HIGH);
    digitalWrite(oscSinSignalLEDpin, LOW);
    break;
  case 3:
    digitalWrite(oscSqrSignalLEDpin, LOW);
    digitalWrite(oscSawSignalLEDpin, LOW);
    digitalWrite(oscSinSignalLEDpin, HIGH);
    break;

  default:
    digitalWrite(oscSqrSignalLEDpin, LOW);
    digitalWrite(oscSawSignalLEDpin, LOW);
    digitalWrite(oscSinSignalLEDpin, LOW);
    break;
  }

  //Detune
  switch (oscDetuneCounter)
  {
  case 1:
    digitalWrite(oscSqrDetuneLEDpin, HIGH);
    digitalWrite(oscSawDetuneLEDpin, LOW);
    digitalWrite(oscSinDetuneLEDpin, LOW);
    break;
  case 2:
    digitalWrite(oscSqrDetuneLEDpin, LOW);
    digitalWrite(oscSawDetuneLEDpin, HIGH);
    digitalWrite(oscSinDetuneLEDpin, LOW);
    break;
  case 3:
    digitalWrite(oscSqrDetuneLEDpin, LOW);
    digitalWrite(oscSawDetuneLEDpin, LOW);
    digitalWrite(oscSinDetuneLEDpin, HIGH);
    break;

  default:
    digitalWrite(oscSqrDetuneLEDpin, LOW);
    digitalWrite(oscSawDetuneLEDpin, LOW);
    digitalWrite(oscSinDetuneLEDpin, LOW);
    break;
  }

  //Delay
  switch (delayParameterCounter)
  {
  case 1:
    digitalWrite(delayTempoLEDpin, HIGH);
    digitalWrite(delayFeedbackLEDpin, LOW);
    digitalWrite(delayMixLEDpin, LOW);
    break;
  case 2:
    digitalWrite(delayTempoLEDpin, LOW);
    digitalWrite(delayFeedbackLEDpin, HIGH);
    digitalWrite(delayMixLEDpin, LOW);
    break;
  case 3:
    digitalWrite(delayTempoLEDpin, LOW);
    digitalWrite(delayFeedbackLEDpin, LOW);
    digitalWrite(delayMixLEDpin, HIGH);
    break;

  default:
    digitalWrite(delayTempoLEDpin, LOW);
    digitalWrite(delayFeedbackLEDpin, LOW);
    digitalWrite(delayMixLEDpin, LOW);
    break;
  }

  //Reverb
  switch (reverbParameterCounter)
  {
  case 1:
    digitalWrite(reverbDryLEDpin, HIGH);
    digitalWrite(reverbWetLEDpin, LOW);
    digitalWrite(reverbWidthLEDpin, LOW);
    digitalWrite(reverbDampingLEDpin, LOW);
    digitalWrite(reverbRoomsizeLEDpin, LOW);
    break;
  case 2:
    digitalWrite(reverbDryLEDpin, LOW);
    digitalWrite(reverbWetLEDpin, HIGH);
    digitalWrite(reverbWidthLEDpin, LOW);
    digitalWrite(reverbDampingLEDpin, LOW);
    digitalWrite(reverbRoomsizeLEDpin, LOW);
    break;
  case 3:
    digitalWrite(reverbDryLEDpin, LOW);
    digitalWrite(reverbWetLEDpin, LOW);
    digitalWrite(reverbWidthLEDpin, HIGH);
    digitalWrite(reverbDampingLEDpin, LOW);
    digitalWrite(reverbRoomsizeLEDpin, LOW);
    break;
  case 4:
    digitalWrite(reverbDryLEDpin, LOW);
    digitalWrite(reverbWetLEDpin, LOW);
    digitalWrite(reverbWidthLEDpin, LOW);
    digitalWrite(reverbDampingLEDpin, HIGH);
    digitalWrite(reverbRoomsizeLEDpin, LOW);
    break;
  case 5:
    digitalWrite(reverbDryLEDpin, LOW);
    digitalWrite(reverbWetLEDpin, LOW);
    digitalWrite(reverbWidthLEDpin, LOW);
    digitalWrite(reverbDampingLEDpin, LOW);
    digitalWrite(reverbRoomsizeLEDpin, HIGH);
    break;

  default:
    digitalWrite(reverbDryLEDpin, LOW);
    digitalWrite(reverbWetLEDpin, LOW);
    digitalWrite(reverbWidthLEDpin, LOW);
    digitalWrite(reverbDampingLEDpin, LOW);
    digitalWrite(reverbRoomsizeLEDpin, LOW);
    break;
  }
}

void filterAnalogInputs()
{
  //LPF Filters
  for (int i = 0; i < 8; i++)
  {
    potsFilterContainerLPF[i].input(potenciometers[i]);
    filteredPotenciometersLPF[i] = potsFilterContainerLPF[i].output();
  }

  //HPF Filters
  for (int i = 0; i < 8; i++)
  {
    potsFilterContainerHPF[i].input(filteredPotenciometersLPF[i]);
    filteredPotenciometersHPF[i] = potsFilterContainerHPF[i].output();
  }
}

void sendParameters()
{

  Serial.println(param2Send);

  //Serial.print("adsr: ");
  //Serial.print(potenciometers[adsrPotIndex]);

  //Serial.print("LPF: ");
  //Serial.print(filteredPotenciometersLPF[reverbPotIndex]);
  //Serial.print(", HPF: ");
  //Serial.println(filteredPotenciometersHPF[reverbPotIndex]);
}

void sendScreenText(String yellowTitle, String blueValue)
{
  oled.clearDisplay();
  oled.setTextColor(WHITE);
  oled.setCursor(0, 0);
  oled.setTextSize(2);
  oled.print(yellowTitle);
  oled.setCursor(0, 25);
  oled.setTextSize(3);
  oled.print(blueValue);
  oled.display();
}